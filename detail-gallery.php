<?php include 'header.php'; ?>
    <section id="single-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h2>Detail Gallery</2>
                        <div class="breadcrumb d-flex align-items-center justify-content-center">
                            <h5>Home</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Gallery</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Detail Gallery</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="gallery-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="detail-gallery">
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="gallery">
                            <ul id="lightSlider">
                                <li data-thumb="https://1.bp.blogspot.com/-q7UW1CoTLgw/TWJw8oZGdLI/AAAAAAAAAe0/qWxTLS26O5I/s1600/German+Shepherd.jpg">
                                  <img src="https://1.bp.blogspot.com/-q7UW1CoTLgw/TWJw8oZGdLI/AAAAAAAAAe0/qWxTLS26O5I/s1600/German+Shepherd.jpg" />
                                </li>
                                <li data-thumb="https://anjingdijual.com/files/jenis-anjing/foto/herder-german-shepherd/german-shepherd-dog.jpg">
                                    <img src="https://anjingdijual.com/files/jenis-anjing/foto/herder-german-shepherd/german-shepherd-dog.jpg" />
                                </li>
                                <li data-thumb="https://www.agsra.com/wp-content/uploads/2020/07/Masalah-Kesehatan-yang-Sering-Dirasakan-oleh-Anjing-German-Shepherd.jpg">
                                    <img src="https://www.agsra.com/wp-content/uploads/2020/07/Masalah-Kesehatan-yang-Sering-Dirasakan-oleh-Anjing-German-Shepherd.jpg" />
                                </li>
                                <li data-thumb="https://upload.wikimedia.org/wikipedia/commons/d/d0/German_Shepherd_-_DSC_0346_%2810096362833%29.jpg">
                                    <img src="https://upload.wikimedia.org/wikipedia/commons/d/d0/German_Shepherd_-_DSC_0346_%2810096362833%29.jpg" />
                                </li>
                            </ul>
                          </div>
                        </div>
                        <div class="col-lg-12">
                          <div class="desc">
                            <h2>Dog Lover Says Her Two German Shepherds Detected Her Breast Cancer</h2>
                            <p>A woman from Wales says she owes her life to her pets after they detected her cancer. Linda Munkley, 65, started checking for breast lumps after noticing her two German shepherds behaving strangely</p>
                            <p>Dog Lover Says Her Two German Shepherds Detected Her Breast Cancer Linda Munkley with her German shepherds, Bea and Enya.</p>
                            <p>"One day I was sat on the sofa when Bea jumped up and began intensely sniffing and headbutting my chest area. She had never done anything like this before so it was quite unusual but at the time I thought nothing of it."</p>
                            <p>However, Linda says, the "headbutting" continued for months.</p>
                            <p>"She kept constantly doing it every day, jumping up at me and really sniffing just my chest area - she was so determined and I couldn't stop her from doing it at all," she explained to Wales Online.</p>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
                <div class="col-lg-4">
                  <div class="category-list">
                    <h3>Browse Images</h3>
                    <ul>
                      <li>
                        <a href="#">All Gallery</a>
                      </li>
                      <li>
                        <a href="#">Your Gallery</a>
                      </li>
                    </ul>
                  </div>
                  <div class="category-list">
                    <h3>Gallery</h3>
                    <ul>
                      <li>
                        <a href="#">Kronos Von Nürburgring</a>
                      </li>
                      <li>
                        <a href="#">Landos Vom Quartier Latin</a>
                      </li>
                      <li>
                        <a href="#">Vegas Du Haut Mansard</a>
                      </li>
                      <li>
                        <a href="#">American's Top Dogs (AKC)</a>
                      </li>
                    </ul>
                  </div>
                </div>
            </div>
        </div>
    </section>
    <?php include 'footer.php'; ?>