<?php include 'header.php'; ?>
    <section id="single-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h2>Pedigree</2>
                        <div class="breadcrumb d-flex align-items-center justify-content-center">
                            <h5>Home</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Pedigree</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="pedigree-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <a href="detail-pedigree.php">
                        <div class="card-gallery">
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                          <div class="desc">
                            <h3>German Shepherds</h3>
                          </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="detail-pedigree.php">
                        <div class="card-gallery">
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/427022.jpg" alt="">
                          <div class="desc">
                            <h3>Gunbil German Shepherds</h3>
                          </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="detail-pedigree.php">
                        <div class="card-gallery">
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/600607324.jpg" alt="">
                          <div class="desc">
                            <h3>Bundessieger Zuchtschau Nurnberg 2011 Males Detail</h3>
                          </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="detail-pedigree.php">
                        <div class="card-gallery">
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/635343823.jpg" alt="">
                          <div class="desc">
                            <h3>Samoyed Dog</h3>
                          </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="detail-pedigree.php">
                        <div class="card-gallery">
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/727651799.jpg" alt="">
                          <div class="desc">
                            <h3>Pit Bull</h3>
                          </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="detail-pedigree.php">
                        <div class="card-gallery">
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/23216140.jpg" alt="">
                          <div class="desc">
                            <h3>Working Dogs</h3>
                          </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-3">
                    <a href="detail-pedigree.php">
                        <div class="card-gallery">
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/260090161.jpg" alt="">
                          <div class="desc">
                            <h3>American's Top Dogs (AKC)</h3>
                          </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-12">
                    <nav class="d-flex justify-content-center">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <?php include 'footer.php'; ?>