<?php include 'header.php'; ?>
    <section id="single-banner">
      <div class="container">
          <div class="row">
              <div class="col-lg-12">
                  <div class="title">
                      <h2>Breeders Detail</2>
                      <div class="breadcrumb d-flex align-items-center justify-content-center">
                          <h5>Home</h5> <i class="uil uil-arrow-right"></i>
                          <h5>Breeders</h5> <i class="uil uil-arrow-right"></i>
                          <h5>Breeders Detail</h5>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </section>
    <section id="breeders-page">
        <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <div class="detail-breeders">
                  <div class="video">
                    <iframe width="100%" height="400" src="https://www.youtube.com/embed/Wb85Ss7SD04" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
                  <div class="title">
                    <h2>Elisa von Team</h2>
                    <p>Elisa von Team G?nbil</p>
                  </div>
                  <div class="card-main-breeders">
                      <div class="title-content">
                        <h2>Izzet Gunbil Gunbil German Shepherd Dogs</h2>
                      </div>
                      <div class="content">
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>Address :</b></p>
                              <p>14360 Arfsten Road</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>Established Date :</b></p>
                              <p>1999</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>Directions :</b></p>
                              <p>Please visit our website</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>Operating Hours :</b></p>
                              <p>Closed Monday</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>City :</b></p>
                              <p>Larkspur</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>Work Telephone :</b></p>
                              <p>-</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>State :</b></p>
                              <p>CO</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>Cell Telephone :</b></p>
                              <p>(719) 220-2222</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>ZIP Code :</b></p>
                              <p>80118</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>Our Website :</b></p>
                              <p><a href="#">Click Here</a></p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>Country :</b></p>
                              <p>USA</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>Email :</b></p>
                              <p>cihan@gunbil.net</p>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="info d-flex flex-wrap justify-content-between">
                              <p><b>Title :</b></p>
                              <p>Admin</p>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  <div class="card-main-breeders">
                    <div class="title-content">
                      <h2>About Us</h2>
                    </div>
                    <div class="content">
                      <p>In our 15 years breeding this magnificent breed, we did not just learn the breeding fundamentals but the expertise in the breeding process, as it is not as simple as you would think, because to become a "successful" breeder and breed to improve and perfecting the breed takes a lot more then an imagination, it is a process that involves understanding canine genetics, recognizing low risk, healthy pedigrees and breeding dogs from these bloodline to ensure a great rate of success in the over-all health of our breeding program, we also focus in the nerve characteristics of each dog that is in our breeding program, the puppies and dog we breed must absolutely have sound temperament and stable nerves. To further clarify, the most crucial elements in breeding is to understand the strength and weakness of your dog/s. In order to be successful in this process you must have a genuine evaluation prepared of the dog/s to be bred, because if you?re not skilled in this procedure you would not be able to choose the mating partner correctly. This is where an evaluation by an expert (a third party) comes in handy as this assessment is done by a breed warden whom is an authority in determining the over-all characteristic in the strengths and faults of your dog. It is essential to have an expert knowledge in the pedigree profiles and genetic report on the lineage (health, temperament, etc), because breeding healthy puppies with correct temperament requires knowledge, not just in the breeding process but identifying the "CORRECT AND COMPATIBLE" " pedigree profile for the breeding pair. This is the reason why a responsible breeder will always have a minimum 5 to 8 "fully titled" studs in their breeding program with strong, healthy and correct pedigrees to compliment the dams (we always have a minimum 5 fully schutzhund titled studs). When you consider your puppy selection from Gunbil German shepherds in Colorado or our kennels in Germany for your family, or the German shepherd show ring and/or a magnificent companion, I can assure you that you are truly getting the best possible German shepherd puppies in the world. We are so confident in our breeding program that we can back-it-up with a full 5 year genetic health and temperament guarantee.! Breeding the German Shepherd is not just a hobby for us, it is our passion, a commitment of excellence, perfection with knowledge, a dedication that start with the breeder and their breeding stock of superior German shepherd males and females. We back it up with a service that supports our character and our integrity. We believe in our German shepherd breeding program, you should too!</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="tab-video">
                  <h3>Upload</h3>
                    <ul class="nav nav-pills" id="pill-myTab" role="tablist">
                      <li class="nav-item"><a class="nav-link active" id="pill-home-tab" data-toggle="tab" href="#videos" role="tab" aria-controls="videos" aria-selected="true">Videos (56)</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#pictures" role="tab" aria-controls="pictures" aria-selected="false">Pictures (32)</a></li>
                    </ul>
                    <div class="tab-content sidebar-list" id="pill-myTabContent">
                      <div class="tab-pane fade show active" id="videos" role="tabpanel" aria-labelledby="home-tab">
                        <a href="#">
                        <div class="list-sidebar d-flex align-items-start">
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/360594374.jpg" alt="">
                          <div class="desc">
                            <small>735 View</small>
                            <h3>Kronos Von Nürburgring Nürburgring</h3>
                            <p>08/06/2014</p>
                          </div>
                        </div>
                        </a>
                        <a href="#">
                          <div class="list-sidebar d-flex align-items-start">
                            <img src="http://germanshepherdkennelclub.com/addons/albums/images/543746282.jpg" alt="">
                            <div class="desc">
                              <small>735 View</small>
                              <h3>Landos Vom Quartier Latin</h3>
                              <p>08/06/2014</p>
                            </div>
                          </div>
                        </a>
                        <a href="#">
                          <div class="list-sidebar d-flex align-items-start">
                            <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                            <div class="desc">
                              <small>735 View</small>
                              <h3>Vegas Du Haut Mansard</h3>
                              <p>08/06/2014</p>
                            </div>
                          </div>
                        </a>
                      </div>
                      <div class="tab-pane fade" id="pictures" role="tabpanel" aria-labelledby="profile-tab">
                        <a href="#">
                          <div class="list-sidebar d-flex align-items-start">
                            <img src="http://germanshepherdkennelclub.com/addons/albums/images/360594374.jpg" alt="">
                            <div class="desc">
                              <small>735 View</small>
                              <h3>Kronos Von Nürburgring Nürburgring</h3>
                              <p>08/06/2014</p>
                            </div>
                          </div>
                          </a>
                          <a href="#">
                            <div class="list-sidebar d-flex align-items-start">
                              <img src="http://germanshepherdkennelclub.com/addons/albums/images/543746282.jpg" alt="">
                              <div class="desc">
                                <small>735 View</small>
                                <h3>Landos Vom Quartier Latin</h3>
                                <p>08/06/2014</p>
                              </div>
                            </div>
                          </a>
                          <a href="#">
                            <div class="list-sidebar d-flex align-items-start">
                              <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                              <div class="desc">
                                <small>735 View</small>
                                <h3>Vegas Du Haut Mansard</h3>
                                <p>08/06/2014</p>
                              </div>
                            </div>
                          </a>
                      </div>
                    </div>
                </div>
              </div>
            </div>
        </div>
    </section>
    <?php include 'footer.php'; ?>