<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!-- ===============================================-->
  <!--    Document Title-->
  <!-- ===============================================-->
  <title>German Shepherd</title>
  <!-- ===============================================-->
  <!--    Favicons-->
  <!-- ===============================================-->
  <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png" />
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png" />
  <link rel="shortcut icon" type="image/x-icon" href="img/favicons/favicon.ico" />
  <link rel="manifest" href="img/favicons/manifest.json" />
  <meta name="msapplication-TileImage" content="img/favicons/mstile-150x150.png" />
  <meta name="theme-color" content="#ffffff" />
  <!-- ===============================================-->
  <!--    Stylesheets-->
  <!-- ===============================================-->
  <script src="js/config.navbar-vertical.js"></script>
  <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin="" />
  <link href="lib/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" />
  <link href="lib/owl.carousel/owl.carousel.css" rel="stylesheet" />
  <link href="css/theme.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.0/css/line.css">
</head>

<body>
  <main class="main" id="top">
    <nav class="navbar navbar-standard navbar-expand-lg fixed-top navbar-dark navbar-theme p-3">
      <div class="container">
        <a class="navbar-brand" href="index.php">
          <span class="text-white"><img src="img/logo-header.png" height="80" style="margin-right: 1rem;" alt=""></span>
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarStandard"
          aria-controls="navbarStandard" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarStandard">
          <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link" href="index.php" aria-haspopup="true" aria-expanded="false">Home</a>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link" href="page.php" aria-haspopup="true" aria-expanded="false">About</a>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link" href="blog.php" aria-haspopup="true" aria-expanded="false">Blog</a>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link" href="#" aria-haspopup="true" aria-expanded="false">Contact</a>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link" href="#" aria-haspopup="true" aria-expanded="false">Advertise</a>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link dropdown-toggle" id="navbarDropdownDocumentation" href="#" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Upload</a>
              <div class="dropdown-menu dropdown-menu-card" aria-labelledby="navbarDropdownDocumentation">
                <div class="bg-white rounded-soft py-2">
                  <a class="dropdown-item" href="#">Add Article</a>
                  <a class="dropdown-item" href="#">Add Images</a>
                  <a class="dropdown-item" href="#">Add Padegree</a>
                  <a class="dropdown-item" href="#">Add Video</a>
                </div>
              </div>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link dropdown-toggle" id="navbarDropdownDocumentation" href="#" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</a>
              <div class="dropdown-menu dropdown-menu-card" aria-labelledby="navbarDropdownDocumentation">
                <div class="bg-white rounded-soft py-2">
                  <a class="dropdown-item" href="pedigree.php">Pedigree</a>
                  <a class="dropdown-item" href="video.php">Video</a>
                  <a class="dropdown-item" href="member.php">Member</a>
                  <a class="dropdown-item" href="breeders.php">Breeder</a>
                  <a class="dropdown-item" href="gallery.php">Images</a>
                </div>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link search-icon" href="#"><span class="fas fa-search"></span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-login" href="#"><span class="fas fa-user"></span>&nbsp; &nbsp;Login or SignUp</a>
            </li>
        </div>
      </div>
    </nav>
    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="py-0 overflow-hidden" id="banner">
      <div class="bg-holder overlay" style="
            background: url(img/bg.jpg);
            background-position: center bottom;
          "></div>
      <!--/.bg-holder-->
      <div class="container">
        <div class="row justify-content-center align-items-center pt-8 pt-lg-10 pb-lg-9 pb-xl-0">
          <div class="col-md-11 col-lg-8 col-xl-4 pb-7 pb-xl-9 text-center text-xl-left">
            <h1 class="text-white font-weight-normal">
              <b><span style="color: #ba7834; font-size: 3rem"><span class="typed-text font-weight-bold"
                    data-typed-text='["German Shepherd"]'></span></span> Kennel Club</b>
            </h1>
            <a class="btn btn-outline-light border-2x rounded-pill btn-lg mt-4 fs-0 py-2" href="#!">Join with us<span
                class="fas fa-play" data-fa-transform="shrink-6 down-1 right-5"></span></a>
          </div>
          <div class="col-xl-7 offset-xl-1 align-self-end">
            <a class="img-landing-banner" href="../index.html">
              <img class="img-fluid" src="img/generic/dashboard-alt3.png" alt="" />
            </a>
          </div>
        </div>
      </div>
      <!-- end of .container-->
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->
    <section id="about-gs">
      <div class="container">
        <div class="row align-items-center justify-content-center mt-4">
          <div class="col-md col-lg-6 col-xl-6 mt-4 mb-md-0">
            <h1 class="fs-2 fs-sm-4 fs-md-5">About Home</h1>
            <p class="lead">German Shepherd breeders for your German shepherd puppies selection, German shepherd puppies
              for sale and German shepherd dogs for sale magazine, exclusively for the German shepherd breeders, German
              shepherd kennel and the German shepherd enthusiasts. If you are looking for German Shepherd puppies,
              German Shepherd Puppies For Sale, German Shepherd Puppy For Sale and naturally German shepherd dogs for
              sale we have it right here from top German Shepherd dog breeders around the world.
            </p>
            <a class="btn btn-outline-light border-2x rounded-pill btn-lg mt-4 fs-0 py-2" href="#!">Read More</a>
          </div>
          <div class="col-md col-lg-6 col-xl-6 pl-lg-6">
            <img class="img-fluid px-6 px-md-0" src="img/gs.svg" alt="German Shepherd" />
          </div>

        </div>
      </div>
      <!-- end of .container-->

    </section>
    <section id="breeders">
      <div class="container">
        <div class="row">
          <div class="col-md col-lg-12 col-xl-12 mt-3 mb-md-0">
            <h1 class="fs-2 fs-sm-4 fs-md-5">Breeders</h1>
            <p class="lead">
              German Shepherd breeders for your German shepherd puppies selection, German shepherd puppies for sale and German shepherd dogs
              for sale magazine, exclusively for the German shepherd breeders, German shepherd kennel and the German shepherd.
            </p>
          </div>
          <div class="col-lg-12">
            <div class="owl-carousel owl-theme owl-theme-dark">
              <a href="#">
                <div class="card-breeder">
                  <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                  <div class="desc">
                    <h3>German Shepherds</h3>
                  </div>
                </div>
              </a>
              <a href="#">
                <div class="card-breeder">
                  <img src="http://germanshepherdkennelclub.com/addons/albums/images/360594374.jpg" alt="">
                  <div class="desc">
                    <h3>German Shepherds Puppies</h3>
                  </div>
                </div>
              </a>
              <a href="#">
                <div class="card-breeder">
                  <img src="http://germanshepherdkennelclub.com/addons/albums/images/595835645.jpg" alt="">
                  <div class="desc">
                    <h3>German Shepherds</h3>
                  </div>
                </div>
              </a>
              <a href="#">
                <div class="card-breeder">
                  <img src="http://germanshepherdkennelclub.com/addons/albums/images/783261167.jpg" alt="">
                  <div class="desc">
                    <h3>German Shepherds</h3>
                  </div>
                </div>
              </a>
            </div>
            </div>
            <div class="col-lg-12">
              <div class="more-card">
                <div class="row">
                  <div class="col-lg col-4">
                    <div class="more">
                      <a href="#">
                        <img src="img/icon-more/pedigree.svg" alt="Pedigree">
                        <h4>Pedigree</h4>
                      </a>
                    </div>
                  </div>
                  <div class="col-lg col-4">
                    <div class="more">
                      <a href="#">
                        <img src="img/icon-more/video.svg" alt="Video">
                        <h4>Video</h4>
                      </a>
                    </div>
                  </div>
                  <div class="col-lg col-4">
                    <div class="more">
                      <a href="#">
                        <img src="img/icon-more/images.svg" alt="Images">
                        <h4>Images</h4>
                      </a>
                    </div>
                  </div>
                  <div class="col-lg col-4">
                    <div class="more">
                      <a href="#">
                        <img src="img/icon-more/member.svg" alt="Member">
                        <h4>Member</h4>
                      </a>
                    </div>
                  </div>
                  <div class="col-lg col-4">
                    <div class="more">
                      <a href="#">
                        <img src="img/icon-more/breeder.svg" alt="Breeder">
                        <h4>Breeder</h4>
                      </a>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="about-gs">
      <div class="container">
        <div class="row align-items-center justify-content-center mt-2">
          <div class="col-md col-lg-6 col-xl-6 pl-lg-6">
            <img class="img-fluid px-6 px-md-0" src="img/gs-2.svg" alt="German Shepherd" />
          </div>
          <div class="col-md col-lg-6 col-xl-6 mt-2 mb-md-0">
            <h1 class="fs-2 fs-sm-4 fs-md-5">Prefereed Dog Breeder</h1>
            <p class="lead">Our well established, high traffic German shepherd dogs and German Shepherd puppies online classifieds are the perfect place to advertise your German Shepherd puppies and German Shepherd dogs for sale. We receive over 1,000.000 (1 Million) hits per month and literally hundreds of people reply to the German shepherd puppies for sale classifieds. If you are a German Shepherd breeder and would like to generate sales, leads or simply recognized on the German Shepherd dogs online magazine.
            </p>
            <a class="btn btn-outline-light border-2x rounded-pill btn-lg mt-4 fs-0 py-2" href="#!">Read More</a>
          </div>

        </div>
      </div>
      <!-- end of .container-->

    </section>

    <section id="breeders">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="backtend">
              <img src="img/blacktend.png" alt="">
            </div>
          </div>
          <div class="col-lg-3 col-6">
              <a href="#">
                <div class="card-breeder">
                  <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                  <div class="desc">
                    <h3>German Shepherds</h3>
                  </div>
                </div>
              </a>
          </div>
          <div class="col-lg-3 col-6">
            <a href="#">
              <div class="card-breeder">
                <img src="http://germanshepherdkennelclub.com/addons/albums/images/360594374.jpg" alt="">
                <div class="desc">
                  <h3>German Shepherds Puppies</h3>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-3 col-6">
            <a href="#">
              <div class="card-breeder">
                <img src="http://germanshepherdkennelclub.com/addons/albums/images/595835645.jpg" alt="">
                <div class="desc">
                  <h3>German Shepherds</h3>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-3 col-6">
            <a href="#">
              <div class="card-breeder">
                <img src="http://germanshepherdkennelclub.com/addons/albums/images/783261167.jpg" alt="">
                <div class="desc">
                  <h3>German Shepherds</h3>
                </div>
              </div>
            </a>
          </div>
          <div class="col-lg-3 col-6">
            <a href="#">
              <div class="card-breeder">
                <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                <div class="desc">
                  <h3>German Shepherds</h3>
                </div>
              </div>
            </a>
        </div>
        <div class="col-lg-3 col-6">
          <a href="#">
            <div class="card-breeder">
              <img src="http://germanshepherdkennelclub.com/addons/albums/images/360594374.jpg" alt="">
              <div class="desc">
                <h3>German Shepherds Puppies</h3>
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-6">
          <a href="#">
            <div class="card-breeder">
              <img src="http://germanshepherdkennelclub.com/addons/albums/images/595835645.jpg" alt="">
              <div class="desc">
                <h3>German Shepherds</h3>
              </div>
            </div>
          </a>
        </div>
        <div class="col-lg-3 col-6">
          <a href="#">
            <div class="card-breeder">
              <img src="http://germanshepherdkennelclub.com/addons/albums/images/783261167.jpg" alt="">
              <div class="desc">
                <h3>German Shepherds</h3>
              </div>
            </div>
          </a>
        </div>
        </div>
      </div>
    </section>

    <section id="newsletter">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="card-newsletter">
              <h1>Join Our Newsletters now!</h1>
              <p class="lead">Subscribe with us for the latest news and updates</p>
              <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <div class="form-newsletter">
                      <div class="form-group d-flex">
                        <input type="email" class="form-control" placeholder="Email address">
                        <button class="btn-form-newsletter">Subscribe</button>
                    </div>
                    </div>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>
    <section class="footer-content pt-8 pb-4">

      <div class="container">
        <div class="position-absolute btn-back-to-top bg-orange"><a class="text-600" href="#banner" data-fancyscroll="data-fancyscroll"><span class="fas fa-chevron-up" style="color: #fff" data-fa-transform="rotate-45"></span></a></div>
        <div class="row">
          <div class="col-lg-6">
            <h5 class="text-uppercase text-white opacity-85 mb-3">German Shepherd Kennel Club</h5>
            <p class="text-600">Please contact us VIA Telephone: (720) 733-0222</p>
            <p>590 Highway 105, Suite 120</p>
            <p>Monument, Colorado. 80132. USA</p>
              
            <div class="icon-group mt-4 mb-4"><a class="icon-item bg-white text-facebook" href="#!"><span class="fab fa-facebook-f"></span></a><a class="icon-item bg-white text-twitter" href="#!"><span class="fab fa-twitter"></span></a><a class="icon-item bg-white text-google-plus" href="#!"><span class="fab fa-google-plus-g"></span></a><a class="icon-item bg-white text-linkedin" href="#!"><span class="fab fa-linkedin-in"></span></a><a class="icon-item bg-white" href="#!"><span class="fab fa-medium-m"></span></a></div>
          </div>
              <div class="col-6 col-lg-3">
                <h5 class="text-uppercase text-white opacity-85 mb-3">Important Links</h5>
                <ul class="list-unstyled">
                  <li class="mb-1"><a class="text-600" href="#!">About</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Site News</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">FAQ</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Contact</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Advertise</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Privacy Policy</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Terms of Use</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Copyright Info</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <h5 class="text-uppercase text-white opacity-85 mb-3">About Club</h5>
                <p>Thank you for using our services at German Shepherd Kennel Club. Our website is dedicated exclusively for the German Shepherd Dog. We are proud GSD breeders, dog trainers, and everything else that is associated with the German Shepherd breed. 
                </p>
              </div>
        </div>
      </div>
      <!-- end of .container-->

    </section>
  </main>
  <!-- ===============================================-->
  <!--    JavaScripts-->
  <!-- ===============================================-->
  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="lib/@fortawesome/all.min.js"></script>
  <script src="lib/stickyfilljs/stickyfill.min.js"></script>
  <script src="lib/sticky-kit/sticky-kit.min.js"></script>
  <script src="lib/is_js/is.min.js"></script>
  <script src="lib/lodash/lodash.min.js"></script>
  <script src="lib/perfect-scrollbar/perfect-scrollbar.js"></script>
  <link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700%7cPoppins:100,200,300,400,500,600,700,800,900&amp;display=swap"
    rel="stylesheet">
  <script src="lib/owl.carousel/owl.carousel.js"></script>
  <script src="lib/typed.js/typed.js"></script>
  <script src="js/theme.js"></script>
  <script>
    $('.owl-carousel').owlCarousel({
    margin:20,
    nav:true,
    autoplay:true,
    autoplayHoverPause:true,
    loop:true,
    dots:false,
    items:4,
    responsive:{
        480:{
            items:2,
            nav:true
        },
        1280:{
            items:4,
            nav:true
        }
    }
})
  </script>
</body>

</html>