<?php include 'header.php'; ?>
    <section id="single-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h2>Blog</2>
                        <div class="breadcrumb d-flex align-items-center justify-content-center">
                            <h5>Home</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Blog</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="list-news d-flex">
                      <img src="https://c.ndtvimg.com/2020-02/qjdup8n4_linda-munkley_625x300_14_February_20.jpg" alt="">
                      <div class="desc">
                        <a href="detail-blog.php"><h2>Dog Lover Says Her Two German Shepherds Detected Her Breast Cancer</h2></a>
                        <p>A woman from Wales says she owes her life to her pets after they detected her cancer. Linda Munkley, 65, started checking for breast lumps after noticing her two German shepherds behaving strangely</p>
                      </div>
                    </div>
                    <div class="list-news d-flex">
                      <img src="https://i.ndtvimg.com/i/2018-03/united-airlines-sends-dog-to-japan_650x400_61521098554.jpg" alt="">
                      <div class="desc">
                        <a href="detail-blog.php"><h2>Family Flies German Shepherd On United Airlines. Gets A Great Dane Back</h2></a>
                        <p>First, there was a report Tuesday that a puppy died on a United Airlines flight after a flight attendant demanded that the dog's owner stow him inside the overhead compartment for the duration of a three-hour flight.</p>
                      </div>
                    </div>
                    <div class="list-news d-flex">
                      <img src="https://i.ndtvimg.com/i/2018-02/dog-unemployment-benefits_650x400_81517570077.jpg" alt="">
                      <div class="desc">
                        <a href="detail-blog.php"><h2>How A Dog Was 'Approved' For $360 Per Week In Unemployment Benefits</h2></a>
                        <p>Just ask Ryder - he was apparently laid off from his job at a restaurant chain in Michigan. So the state approved him for $360 per week in unemployment benefits.</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-lg-12">
                        <nav class="d-flex justify-content-center">
                          <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                          </ul>
                        </nav>
                      </div>
                    </div>
                </div>
                <div class="col-lg-4">
                  <div class="category-list">
                    <h3>Category</h3>
                    <ul>
                      <li>
                        <a href="#">People</a>
                      </li>
                      <li>
                        <a href="#">Places</a>
                      </li>
                      <li>
                        <a href="#">Personal</a>
                      </li>
                      <li>
                        <a href="#">Dogs</a>
                      </li>
                      <li>
                        <a href="#">Pet Products</a>
                      </li>
                      <li>
                        <a href="#">Pet Food</a>
                      </li>
                    </ul>
                  </div>
                </div>
            </div>
        </div>
    </section>
<?php include 'footer.php'; ?>