<?php include 'header.php'; ?>
    <section id="single-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h2>Members</2>
                        <div class="breadcrumb d-flex align-items-center justify-content-center">
                            <h5>Home</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Members</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="member-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-6">
                  <a href="detail-breeders.php">
                    <div class="member-card"><img class="shadow-sm" src="img/generic/2.jpg" alt="" />
                      <h6 class="mb-1">Sophie Turner</h6>
                      <p>Graduate Student Council</p>
                    </div>
                  </a>
                </div>
                <div class="col-lg-3 col-6">
                  <a href="detail-breeders.php">
                    <div class="member-card"><img class="shadow-sm" src="img/generic/2.jpg" alt="" />
                      <h6 class="mb-1">Sophie Turner</h6>
                      <p>Graduate Student Council</p>
                    </div>
                  </a>
                </div>
                <div class="col-lg-3 col-6">
                  <a href="detail-breeders.php">
                    <div class="member-card"><img class="shadow-sm" src="img/generic/2.jpg" alt="" />
                      <h6 class="mb-1">Sophie Turner</h6>
                      <p>Graduate Student Council</p>
                    </div>
                  </a>
                </div>
                <div class="col-lg-3 col-6">
                  <a href="detail-breeders.php">
                    <div class="member-card"><img class="shadow-sm" src="img/generic/2.jpg" alt="" />
                      <h6 class="mb-1">Sophie Turner</h6>
                      <p>Graduate Student Council</p>
                    </div>
                  </a>
                </div>
                <div class="col-lg-3 col-6">
                  <a href="detail-breeders.php">
                    <div class="member-card"><img class="shadow-sm" src="img/generic/2.jpg" alt="" />
                      <h6 class="mb-1">Sophie Turner</h6>
                      <p>Graduate Student Council</p>
                    </div>
                  </a>
                </div>
                <div class="col-lg-12">
                    <nav class="d-flex justify-content-center">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <?php include 'footer.php'; ?>