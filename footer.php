<section class="footer-content pt-8 pb-4">
      <div class="container">
        <div class="position-absolute btn-back-to-top bg-orange"><a class="text-600" href="#banner" data-fancyscroll="data-fancyscroll"><span class="fas fa-chevron-up" style="color: #fff" data-fa-transform="rotate-45"></span></a></div>
        <div class="row">
          <div class="col-lg-6">
            <h5 class="text-uppercase text-white opacity-85 mb-3">German Shepherd Kennel Club</h5>
            <p class="text-600">Please contact us VIA Telephone: (720) 733-0222</p>
            <p>590 Highway 105, Suite 120</p>
            <p>Monument, Colorado. 80132. USA</p>
              
            <div class="icon-group mt-4 mb-4"><a class="icon-item bg-white text-facebook" href="#!"><span class="fab fa-facebook-f"></span></a><a class="icon-item bg-white text-twitter" href="#!"><span class="fab fa-twitter"></span></a><a class="icon-item bg-white text-google-plus" href="#!"><span class="fab fa-google-plus-g"></span></a><a class="icon-item bg-white text-linkedin" href="#!"><span class="fab fa-linkedin-in"></span></a><a class="icon-item bg-white" href="#!"><span class="fab fa-medium-m"></span></a></div>
          </div>
              <div class="col-6 col-lg-3">
                <h5 class="text-uppercase text-white opacity-85 mb-3">Important Links</h5>
                <ul class="list-unstyled">
                  <li class="mb-1"><a class="text-600" href="#!">About</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Site News</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">FAQ</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Contact</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Advertise</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Privacy Policy</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Terms of Use</a></li>
                  <li class="mb-1"><a class="text-600" href="#!">Copyright Info</a></li>
                </ul>
              </div>
              <div class="col-lg-3">
                <h5 class="text-uppercase text-white opacity-85 mb-3">About Club</h5>
                <p>Thank you for using our services at German Shepherd Kennel Club. Our website is dedicated exclusively for the German Shepherd Dog. We are proud GSD breeders, dog trainers, and everything else that is associated with the German Shepherd breed. 
                </p>
              </div>
        </div>
      </div>
      <!-- end of .container-->

    </section>
  </main>
  <!-- ===============================================-->
  <!--    JavaScripts-->
  <!-- ===============================================-->

  <script src="js/jquery.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="lib/@fortawesome/all.min.js"></script>
  <script src="lib/stickyfilljs/stickyfill.min.js"></script>
  <script src="lib/sticky-kit/sticky-kit.min.js"></script>
  <script src="lib/is_js/is.min.js"></script>
  <script src="lib/lodash/lodash.min.js"></script>
  <script src="lib/perfect-scrollbar/perfect-scrollbar.js"></script>
  <link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700%7cPoppins:100,200,300,400,500,600,700,800,900&amp;display=swap"
    rel="stylesheet">
  <script src="lib/owl.carousel/owl.carousel.js"></script>
  <script src="lib/typed.js/typed.js"></script>
  <script src="js/theme.js"></script>
  <script src="https://sachinchoolur.github.io/lightslider/dist/js/lightslider.js"></script>

  <script>
    $('.owl-carousel').owlCarousel({
    margin:20,
    nav:true,
    autoplay:true,
    autoplayHoverPause:true,
    loop:true,
    dots:false,
    items:4,
    responsive:{
        480:{
            items:2,
            nav:true
        },
        1280:{
            items:4,
            nav:true
        }
    }
});

$('#lightSlider').lightSlider({
    gallery: true,
    item: 1,
    loop:true,
    slideMargin: 0,
    thumbItem: 9
});
  </script>
</body>

</html>