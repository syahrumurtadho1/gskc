<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!-- ===============================================-->
  <!--    Document Title-->
  <!-- ===============================================-->
  <title>German Shepherd</title>
  <!-- ===============================================-->
  <!--    Favicons-->
  <!-- ===============================================-->
  <link rel="apple-touch-icon" sizes="180x180" href="img/favicons/apple-touch-icon.png" />
  <link rel="icon" type="image/png" sizes="32x32" href="img/favicons/favicon-32x32.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="img/favicons/favicon-16x16.png" />
  <link rel="shortcut icon" type="image/x-icon" href="img/favicons/favicon.ico" />
  <link rel="manifest" href="img/favicons/manifest.json" />
  <meta name="msapplication-TileImage" content="img/favicons/mstile-150x150.png" />
  <meta name="theme-color" content="#ffffff" />
  <!-- ===============================================-->
  <!--    Stylesheets-->
  <!-- ===============================================-->
  <link rel="stylesheet" href="https://sachinchoolur.github.io/lightslider/dist/css/lightslider.css">

  <script src="js/config.navbar-vertical.js"></script>
  <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin="" />
  <link href="lib/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" />
  <link href="lib/owl.carousel/owl.carousel.css" rel="stylesheet" />
  <link href="css/theme.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.0/css/line.css">
  
</head>
<body>
  <main class="main" id="top">
    <nav class="navbar navbar-standard navbar-expand-lg fixed-top navbar-dark navbar-theme p-3 not-sticky-black">
      <div class="container">
        <a class="navbar-brand" href="index.php">
          <span class="text-white"><img src="img/logo-header.png" height="80" style="margin-right: 1rem;" alt=""></span>
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarStandard"
          aria-controls="navbarStandard" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarStandard">
          <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link" href="index.php" aria-haspopup="true" aria-expanded="false">Home</a>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link" href="page.php" aria-haspopup="true" aria-expanded="false">About</a>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link" href="blog.php" aria-haspopup="true" aria-expanded="false">Blog</a>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link" href="#" aria-haspopup="true" aria-expanded="false">Contact</a>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link" href="#" aria-haspopup="true" aria-expanded="false">Advertise</a>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link dropdown-toggle" id="navbarDropdownDocumentation" href="#" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Upload</a>
              <div class="dropdown-menu dropdown-menu-card" aria-labelledby="navbarDropdownDocumentation">
                <div class="bg-white rounded-soft py-2">
                  <a class="dropdown-item" href="#">Add Article</a>
                  <a class="dropdown-item" href="#">Add Images</a>
                  <a class="dropdown-item" href="#">Add Padegree</a>
                  <a class="dropdown-item" href="#">Add Video</a>
                </div>
              </div>
            </li>
            <li class="nav-item dropdown dropdown-on-hover">
              <a class="nav-link dropdown-toggle" id="navbarDropdownDocumentation" href="#" role="button"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</a>
              <div class="dropdown-menu dropdown-menu-card" aria-labelledby="navbarDropdownDocumentation">
                <div class="bg-white rounded-soft py-2">
                  <a class="dropdown-item" href="pedigree.php">Pedigree</a>
                  <a class="dropdown-item" href="video.php">Video</a>
                  <a class="dropdown-item" href="member.php">Member</a>
                  <a class="dropdown-item" href="breeders.php">Breeder</a>
                  <a class="dropdown-item" href="gallery.php">Images</a>
                </div>
              </div>
            </li>
          </ul>
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link search-icon" href="#"><span class="fas fa-search"></span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-login" href="#"><span class="fas fa-user"></span>&nbsp; &nbsp;Login or SignUp</a>
            </li>
        </div>
      </div>
    </nav>