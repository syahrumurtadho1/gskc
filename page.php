<?php include 'header.php'; ?>
    <section id="single-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h2>About Us</2>
                        <div class="breadcrumb d-flex align-items-center justify-content-center">
                            <h5>Home</h5> <i class="uil uil-arrow-right"></i>
                            <h5>About Us</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="single-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Thank you for using our FREE services at <b>German Shepherd Kennel Club.</b>
                </div>
            </div>
        </div>
    </section>
  <?php include 'footer.php'; ?>