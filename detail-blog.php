<?php include 'header.php'; ?>
    <section id="single-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h2>Detail Blog</2>
                        <div class="breadcrumb d-flex align-items-center justify-content-center">
                            <h5>Home</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Blog</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Detail Blog</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="blog-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="detail-news">
                      <img src="https://c.ndtvimg.com/2020-02/qjdup8n4_linda-munkley_625x300_14_February_20.jpg" alt="">
                      <div class="desc">
                        <h2>Dog Lover Says Her Two German Shepherds Detected Her Breast Cancer</h2>
                        <p>A woman from Wales says she owes her life to her pets after they detected her cancer. Linda Munkley, 65, started checking for breast lumps after noticing her two German shepherds behaving strangely</p>
                        <p>Dog Lover Says Her Two German Shepherds Detected Her Breast Cancer Linda Munkley with her German shepherds, Bea and Enya.</p>
                        <p>"One day I was sat on the sofa when Bea jumped up and began intensely sniffing and headbutting my chest area. She had never done anything like this before so it was quite unusual but at the time I thought nothing of it."</p>
                        <p>However, Linda says, the "headbutting" continued for months.</p>
                        <p>"She kept constantly doing it every day, jumping up at me and really sniffing just my chest area - she was so determined and I couldn't stop her from doing it at all," she explained to Wales Online.</p>
                      </div>
                    </div>
                </div>
                <div class="col-lg-4">
                  <div class="category-list">
                    <h3>Category</h3>
                    <ul>
                      <li>
                        <a href="#">People</a>
                      </li>
                      <li>
                        <a href="#">Places</a>
                      </li>
                      <li>
                        <a href="#">Personal</a>
                      </li>
                      <li>
                        <a href="#">Dogs</a>
                      </li>
                      <li>
                        <a href="#">Pet Products</a>
                      </li>
                      <li>
                        <a href="#">Pet Food</a>
                      </li>
                    </ul>
                  </div>
                </div>
            </div>
        </div>
    </section>
<?php include 'footer.php'; ?>