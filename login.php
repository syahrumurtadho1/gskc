<?php include 'header.php'; ?>
    <section id="single-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h2>Login</2>
                        <div class="breadcrumb d-flex align-items-center justify-content-center">
                            <h5>Home</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Login</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="login-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 offset-lg-3">
                    <div class="card card-black">
                        <div class="card-body p-4 p-sm-5">
                            <div class="row text-left justify-content-between align-items-center mb-2">
                            <div class="col-auto">
                                <h5>Log in</h5>
                            </div>
                            <div class="col-auto">
                                <p class="fs--1 text-600 mb-0">or <a href="#">Create an account</a></p>
                            </div>
                            </div>
                            <form>
                            <div class="form-group">
                                <input class="form-control" type="email" placeholder="Email address" />
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" placeholder="Password" />
                            </div>
                            <div class="row justify-content-between align-items-center">
                                <div class="col-auto">
                                    <div class="col-auto"><a class="fs--1" href="#">Forgot Password?</a></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary btn-block mt-3" type="submit" name="submit">Log in</button>
                            </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  <?php include 'footer.php'; ?>