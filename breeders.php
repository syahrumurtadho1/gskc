<?php include 'header.php'; ?>
    <section id="single-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h2>Breeders</2>
                        <div class="breadcrumb d-flex align-items-center justify-content-center">
                            <h5>Home</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Breeders</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="breeders-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <a href="detail-breeders.php">
                        <div class="card-breeders">
                          <div class="image">
                            <div class="watch-video"><i class="uil uil-play"></i>Watch Video</div>
                            <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                          </div>
                          <div class="desc">
                            <h3>German Shepherds</h3>
                            <p>In our 15 years breeding this magnificent breed, we did not just learn the breeding fundamentals but the expertise in the breeding process, as it is not as simple as you would think</p>
                          </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                    <a href="detail-breeders.php">
                        <div class="card-breeders">
                          <div class="image">
                            <div class="watch-video"><i class="uil uil-play"></i>Watch Video</div>
                            <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                          </div>
                          <div class="desc">
                            <h3>Milena Kirchner</h3>
                            <p>In our 15 years breeding this magnificent breed, we did not just learn the breeding fundamentals but the expertise in the breeding process, as it is not as simple as you would think</p>
                          </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4">
                  <a href="detail-breeders.php">
                      <div class="card-breeders">
                        <div class="image">
                          <div class="watch-video"><i class="uil uil-play"></i>Watch Video</div>
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                        </div>
                        <div class="desc">
                          <h3>Jerzy Pawlik</h3>
                          <p>In our 15 years breeding this magnificent breed, we did not just learn the breeding fundamentals but the expertise in the breeding process, as it is not as simple as you would think</p>
                        </div>
                      </div>
                  </a>
                </div>
                <div class="col-lg-4">
                  <a href="detail-breeders.php">
                      <div class="card-breeders">
                        <div class="image">
                          <div class="watch-video"><i class="uil uil-play"></i>Watch Video</div>
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                        </div>
                        <div class="desc">
                          <h3>German Shepherds</h3>
                          <p>In our 15 years breeding this magnificent breed, we did not just learn the breeding fundamentals but the expertise in the breeding process, as it is not as simple as you would think</p>
                        </div>
                      </div>
                  </a>
                </div>
                <div class="col-lg-4">
                  <a href="detail-breeders.php">
                      <div class="card-breeders">
                        <div class="image">
                          <div class="watch-video"><i class="uil uil-play"></i>Watch Video</div>
                          <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                        </div>
                        <div class="desc">
                          <h3>Von Haus Gaea</h3>
                          <p>In our 15 years breeding this magnificent breed, we did not just learn the breeding fundamentals but the expertise in the breeding process, as it is not as simple as you would think</p>
                        </div>
                      </div>
                  </a>
                </div>
                <div class="col-lg-12">
                    <nav class="d-flex justify-content-center">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <?php include 'footer.php'; ?>