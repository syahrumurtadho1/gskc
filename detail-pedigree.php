<?php include 'header.php'; ?>
    <section id="single-banner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="title">
                        <h2>Detail Pedigree</2>
                        <div class="breadcrumb d-flex align-items-center justify-content-center">
                            <h5>Home</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Pedigree</h5> <i class="uil uil-arrow-right"></i>
                            <h5>Detail Pedigree</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="pedigree-page">
        <div class="container">
          <div class="detail-pedigree">
            <div class="row">
                <div class="col-lg-6">
                  <div class="gallery">
                    <ul id="lightSlider">
                        <li data-thumb="https://1.bp.blogspot.com/-q7UW1CoTLgw/TWJw8oZGdLI/AAAAAAAAAe0/qWxTLS26O5I/s1600/German+Shepherd.jpg">
                          <img src="https://1.bp.blogspot.com/-q7UW1CoTLgw/TWJw8oZGdLI/AAAAAAAAAe0/qWxTLS26O5I/s1600/German+Shepherd.jpg" />
                        </li>
                        <li data-thumb="https://anjingdijual.com/files/jenis-anjing/foto/herder-german-shepherd/german-shepherd-dog.jpg">
                            <img src="https://anjingdijual.com/files/jenis-anjing/foto/herder-german-shepherd/german-shepherd-dog.jpg" />
                        </li>
                        <li data-thumb="https://www.agsra.com/wp-content/uploads/2020/07/Masalah-Kesehatan-yang-Sering-Dirasakan-oleh-Anjing-German-Shepherd.jpg">
                            <img src="https://www.agsra.com/wp-content/uploads/2020/07/Masalah-Kesehatan-yang-Sering-Dirasakan-oleh-Anjing-German-Shepherd.jpg" />
                        </li>
                        <li data-thumb="https://upload.wikimedia.org/wikipedia/commons/d/d0/German_Shepherd_-_DSC_0346_%2810096362833%29.jpg">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/d/d0/German_Shepherd_-_DSC_0346_%2810096362833%29.jpg" />
                        </li>
                    </ul>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="card-main-pedigree">
                    <div class="title-content">
                      <h2>Quenny von Gunbil Imports <i class="uil uil-venus"></i> </h2>
                    </div>
                    <div class="content">
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="info d-flex flex-wrap justify-content-between">
                            <p><b>Category :</b></p>
                            <p>German Shepherd Dogs</p>
                          </div>
                          <div class="info d-flex flex-wrap justify-content-between">
                            <p><b>Reg. No. 1	 :</b></p>
                            <p>MCHIP 981189900108638</p>
                          </div>
                          <div class="info d-flex flex-wrap justify-content-between">
                            <p><b>Reg. No. 2 :</b></p>
                            <p>N/A</p>
                          </div>
                          <div class="info d-flex flex-wrap justify-content-between">
                            <p><b>DOB :</b></p>
                            <p>March 23, 2020</p>
                          </div>
                          <div class="info d-flex flex-wrap justify-content-between">
                            <p><b>Tatto :</b></p>
                            <p>N/A</p>
                          </div>
                          <div class="info d-flex flex-wrap justify-content-between">
                            <p><b>Micro Chip :</b></p>
                            <p>981189900108638</p>
                          </div>
                          <div class="info d-flex flex-wrap justify-content-between">
                            <p><b>Coat Type :</b></p>
                            <p>Stock Coat (Stockhaar)</p>
                          </div>
                          <div class="info d-flex flex-wrap justify-content-between">
                            <p><b>Color :</b></p>
                            <p>	Black/Red</p>
                          </div>
                          <div class="info d-flex flex-wrap justify-content-between">
                            <p><b>Title :</b></p>
                            <p>N/A</p>
                          </div>
                          <div class="info d-flex flex-wrap justify-content-between">
                            <p><b>Show Rank :</b></p>
                            <p>	N/A</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-12">
                  <div class="tab-pedigree">
                    <ul class="nav nav-pills" id="pill-myTab" role="tablist">
                      <li class="nav-item"><a class="nav-link active" id="pill-home-tab" data-toggle="tab" href="#pedigree" role="tab" aria-controls="pedigree" aria-selected="true">Pedigree</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#videos" role="tab" aria-controls="videos" aria-selected="false">Videos</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="comments" aria-selected="false">Comments</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#progeny" role="tab" aria-controls="progeny" aria-selected="false">Progeny</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#siblings" role="tab" aria-controls="siblings" aria-selected="false">Siblings</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#shows" role="tab" aria-controls="shows" aria-selected="false">Shows</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#mating" role="tab" aria-controls="mating" aria-selected="false">Mating</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#deworming" role="tab" aria-controls="deworming" aria-selected="false">Deworming</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#rabies" role="tab" aria-controls="rabies" aria-selected="false">Rabies</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#vaccines" role="tab" aria-controls="vaccines" aria-selected="false">Vaccines</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#health" role="tab" aria-controls="health" aria-selected="false">Health</a></li>
                      <li class="nav-item"><a class="nav-link" id="pill-profile-tab" data-toggle="tab" href="#litters" role="tab" aria-controls="litters" aria-selected="false">Litters</a></li>
                    </ul>
                    <div class="tab-content sidebar-list" id="pill-myTabContent">
                      <div class="tab-pane fade show active" id="pedigree" role="tabpanel" aria-labelledby="pedigree-tab">
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="card-main-pedigree">
                              <div class="title-content">
                                <h2>Genetic Health</h2>
                              </div>
                              <div class="content">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Hips (HD) :</b></p>
                                      <p>N/A</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Elbows (ED) :</b></p>
                                      <p>N/A</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Breed Value :</b></p>
                                      <p>80</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>DM :</b></p>
                                      <p>N/A</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>DNA Profile :</b></p>
                                      <p>N/A</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="card-main-pedigree">
                              <div class="title-content">
                                <h2>Anatomy Data</h2>
                              </div>
                              <div class="content">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Date :</b></p>
                                      <p>N/A</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Height/Withers :</b></p>
                                      <p>N/A</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Breast Depth :</b></p>
                                      <p>N/A</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Breast Width :</b></p>
                                      <p>N/A</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Weight :</b></p>
                                      <p>N/A</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="card-main-pedigree">
                              <div class="title-content">
                                <h2>Line Breeding</h2>
                              </div>
                              <div class="content">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><a href="#">VA1 Remo vom Fichtenschlag ... 3-3</a></p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><a href="#">SG46 Ray vom Fichtenschlag ... 4-4</a></p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><a href="#">SG5 Thora vom Fichtenschlag ... 4-4</a></p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><a href="#">VA1 Zamp vom Thermodos ... 4-3</a></p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><a href="#">V33 Carolin vom Fichtenschlag ... 5-5</a></p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><a href="#">T2 Wilko vom Fichtenschlag ... 5-5</a></p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="card-main-pedigree">
                              <div class="title-content">
                                <h2>Owner / Breeder Information</h2>
                              </div>
                              <div class="content">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><a href="#">Izzet Gunbil Gunbil German Shepherd Dogs</a></p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <div class="card-main-pedigree">
                              <div class="title-content">
                                <h2>Koer Report</h2>
                              </div>
                              <div class="content">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <p>Vaccines:</p>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p>07/31/2020 - Versican DHPPi Plus L4</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p>07/31/2020 - Versican DHPPi Plus L4</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <h3>Parents :</h3>
                          </div>
                          <div class="col-lg-6">
                            <div class="card-main-pedigree">
                              <div class="title-content">
                                <h2>Sire: VA1 Gary vom Hühnegrab</h2>
                              </div>
                              <div class="content">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="parent-image">
                                      <img src="http://germanshepherdkennelclub.com/pictures/6j3jL1Juj3hjm7ichsdINZYRZGlIAIQ7.jpg" alt="">
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Registration# :</b></p>
                                      <p>SZ2293412</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Gender :</b></p>
                                      <p>Male</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Birth Date :</b></p>
                                      <p>March 17, 2013</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Color :</b></p>
                                      <p>sbA (Black, Brown ma</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Title :</b></p>
                                      <p>IPO-3/Kkl1</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Show Rank :</b></p>
                                      <p>VA1</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Breed Value :</b></p>
                                      <p>79</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="card-main-pedigree">
                              <div class="title-content">
                                <h2>Dam: V8 Bonny vom Haus Gunbil Rochele</h2>
                              </div>
                              <div class="content">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <div class="parent-image">
                                      <img src="http://germanshepherdkennelclub.com/pictures/gVO5GtmL6LhOMKwXjF8WJ6VKa7iaQoKw.jpg" alt="">
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Registration# :</b></p>
                                      <p>SZ2326917</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Gender :</b></p>
                                      <p>Female</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Birth Date :</b></p>
                                      <p>June 15, 2016</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Color :</b></p>
                                      <p>Blk/Red</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Title :</b></p>
                                      <p>IPO-1/Kkl1</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Show Rank :</b></p>
                                      <p>V8</p>
                                    </div>
                                    <div class="info d-flex flex-wrap justify-content-between">
                                      <p><b>Breed Value :</b></p>
                                      <p>81</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-12">
                            <h3>Grand Parents :</h3>
                          </div>
                          <div class="col-lg-12 h-100">
                            <div class="card-grand d-flex align-items-center">
                              <div class="cell-1">
                                <div class="subcell-1">
                                  <img src="http://germanshepherdkennelclub.com/pictures/nqcJAhyTkQKPr62TuE2i1Db8IufbBwoo.jpg" alt="">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                  <p>SchH3/Kkl1</p>
                                </div>
                              </div>
                              <div class="cell-2">
                                <div class="subcell-2">
                                  <img src="http://germanshepherdkennelclub.com/pictures/nqcJAhyTkQKPr62TuE2i1Db8IufbBwoo.jpg" alt="">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                  <p>SchH3/Kkl1</p>
                                </div>
                                <div class="subcell-2">
                                  <img src="http://germanshepherdkennelclub.com/pictures/nqcJAhyTkQKPr62TuE2i1Db8IufbBwoo.jpg" alt="">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                  <p>SchH3/Kkl1</p>
                                </div>
                              </div>
                              <div class="cell-3">
                                <div class="subcell-3">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-3">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-3">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-3">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                              </div>
                              <div class="cell-4">
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                              </div>
                            </div>
                            <div class="card-grand d-flex align-items-center">
                              <div class="cell-1">
                                <div class="subcell-1">
                                  <img src="http://germanshepherdkennelclub.com/pictures/nqcJAhyTkQKPr62TuE2i1Db8IufbBwoo.jpg" alt="">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                  <p>SchH3/Kkl1</p>
                                </div>
                              </div>
                              <div class="cell-2">
                                <div class="subcell-2">
                                  <img src="http://germanshepherdkennelclub.com/pictures/nqcJAhyTkQKPr62TuE2i1Db8IufbBwoo.jpg" alt="">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                  <p>SchH3/Kkl1</p>
                                </div>
                                <div class="subcell-2">
                                  <img src="http://germanshepherdkennelclub.com/pictures/nqcJAhyTkQKPr62TuE2i1Db8IufbBwoo.jpg" alt="">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                  <p>SchH3/Kkl1</p>
                                </div>
                              </div>
                              <div class="cell-3">
                                <div class="subcell-3">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-3">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-3">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-3">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                              </div>
                              <div class="cell-4">
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                                <div class="subcell-4">
                                  <h4>VA1 Omen vom Radhaus Radhaus Radhaus</h4>
                                  <a href="#">SZ: 9159530</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="videos" role="tabpanel" aria-labelledby="videos-tab">
                        <div class="row">
                          <div class="col-lg-4">
                            <a href="detail-breeders.php">
                                <div class="card-video">
                                  <div class="image">
                                    <div class="watch-video"><i class="uil uil-play"></i>Watch Video</div>
                                    <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                                  </div>
                                  <div class="desc">
                                    <h3>Jerzy Pawlik</h3>
                                  </div>
                                </div>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="comments" role="tabpanel" aria-labelledby="comments-tab">
                        <div class="comment-pedigree">
                          <div class="media btn-reveal-trigger"><img class="img-fluid" src="https://images.unsplash.com/photo-1543466835-00a7907e9de1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=667&q=80" alt="" />
                            <div class="media-body position-relative pl-3">
                              <h6 class="fs-0 mb-1"><a href="#!">Media Heading</a></h6>
                              <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.</p>
                            </div>
                          </div>
                          <div class="form-comment">
                            <h3>Add Comment</h3>
                            <textarea class="form-control" rows="4"></textarea>
                            <button>Submit</button>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="progeny" role="tabpanel" aria-labelledby="progeny-tab">
                        <div class="row">
                          <div class="col-lg-4">
                            <a href="detail-breeders.php">
                                <div class="card-video">
                                  <div class="image">
                                    <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                                  </div>
                                  <div class="desc">
                                    <h3>Jerzy Pawlik</h3>
                                    <p>Father</p>
                                  </div>
                                </div>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="siblings" role="tabpanel" aria-labelledby="siblings-tab">
                        <div class="row">
                          <div class="col-lg-6">
                            <div class="card-main-pedigree">
                              <div class="title-content">
                                <h2>Quennyvon Gunbil Imports</h2>
                              </div>
                              <div class="content">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <a href="#">
                                      <div class="full-image">
                                        <img src="http://germanshepherdkennelclub.com/pictures/quenny1.jpg" alt="">
                                      </div>
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-6">
                            <div class="card-main-pedigree">
                              <div class="title-content">
                                <h2>Quenn vom Haus Gunbil Rochele</h2>
                              </div>
                              <div class="content">
                                <div class="row">
                                  <div class="col-lg-12">
                                    <a href="#">
                                      <div class="full-image">
                                        <img src="http://germanshepherdkennelclub.com/pictures/quenn-gar-son.jpg" alt="">
                                      </div>
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="shows" role="tabpanel" aria-labelledby="shows-tab">
                        <div class="row">
                          <div class="col-lg-12">
                            <table class="table table-dark">
                              <thead>
                                <tr>
                                  <th scope="col">Show</th>
                                  <th scope="col">Country</th>
                                  <th scope="col">Judge</th>
                                  <th scope="col">Place</th>
                                  <th class="white-space-nowrap" scope="col">Rank</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th scope="row">2019 LG08</th>
                                  <td>DEU</td>
                                  <td>D. Oeser</td>
                                  <td>V</td>
                                  <td class="white-space-nowrap">2</td>
                                </tr>
                                <tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="mating" role="tabpanel" aria-labelledby="mating-tab">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-mating">
                              <i class="uil uil-search"></i>
                              <input class="form-control" id="name" type="text" placeholder="Enter the name or regcode of the dog you would like to breed with Robby">
                            </div>
                          </div>
                          <div class="col-lg-4">
                            <a href="detail-breeders.php">
                                <div class="card-video">
                                  <div class="image">
                                    <img src="http://germanshepherdkennelclub.com/addons/albums/images/473208197.jpg" alt="">
                                  </div>
                                  <div class="desc">
                                    <h3>Jerzy Pawlik</h3>
                                    <button class="btn-process">Process</button>
                                  </div>
                                </div>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="deworming" role="tabpanel" aria-labelledby="deworming-tab">
                        <div class="row">
                          <div class="col-lg-12">
                            <table class="table table-dark">
                              <thead>
                                <tr>
                                  <th scope="col">Insert Date</th>
                                  <th scope="col">Name</th>
                                  <th scope="col">Dosage</th>
                                  <th scope="col">Due Data</th>
                                  <th class="white-space-nowrap" scope="col">Weight</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th scope="row">June 20, 2020</th>
                                  <td>Pyrantel Pamoate Suspension	</td>
                                  <td>1.5 ML</td>
                                  <td>July 8, 2020	</td>
                                  <td class="white-space-nowrap">-</td>
                                </tr>
                                <tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="rabies" role="tabpanel" aria-labelledby="rabies-tab">
                        <div class="row">
                          <div class="col-lg-12">
                            <table class="table table-dark">
                              <thead>
                                <tr>
                                  <th scope="col">Insert Date</th>
                                  <th scope="col">Name</th>
                                  <th scope="col">Dosage</th>
                                  <th scope="col">Due Data</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th scope="row">June 20, 2020</th>
                                  <td>Versican Plus L4R</td>
                                  <td>1ML / 56602601 EXP: 06/06/2021</td>
                                  <td>June 13, 2020</td>
                                </tr>
                                <tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="vaccines" role="tabpanel" aria-labelledby="vaccines-tab">
                        <div class="row">
                          <div class="col-lg-12">
                            <table class="table table-dark">
                              <thead>
                                <tr>
                                  <th scope="col">Insert Date</th>
                                  <th scope="col">Name</th>
                                  <th scope="col">Dosage</th>
                                  <th scope="col">Due Data</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th scope="row">June 20, 2020</th>
                                  <td>Nobivac Canine 1-DAPPvL2</td>
                                  <td>1ML</td>
                                  <td>June 29, 2021</td>
                                </tr>
                                <tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="health" role="tabpanel" aria-labelledby="health-tab">
                        <div class="row">
                          <div class="col-lg-12">
                            <table class="table table-dark">
                              <thead>
                                <tr>
                                  <th scope="col">Insert Date</th>
                                  <th scope="col">Name</th>
                                  <th scope="col">Dosage</th>
                                  <th scope="col">Due Data</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <th scope="row">June 20, 2020</th>
                                  <td>Metronidazole</td>
                                  <td>250ML / 10 Days</td>
                                  <td>June 13, 2020</td>
                                </tr>
                                <tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="litters" role="tabpanel" aria-labelledby="litters-tab">
                        <div class="row">
                          <div class="col-lg-12">
                            No record found
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </section>
    <?php include 'footer.php'; ?>